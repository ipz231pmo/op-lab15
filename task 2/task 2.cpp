﻿#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define ARRAY_SIZE_ROWS 5
#define ARRAY_SIZE_COLS 10
int array[ARRAY_SIZE_ROWS][ARRAY_SIZE_COLS];
void randomizeArray() {
	for (int i = 0; i < ARRAY_SIZE_ROWS; i++)
		for (int j = 0; j < ARRAY_SIZE_COLS; j++) {
			array[i][j] = rand() % 201 - 100;
#ifdef _DEBUG
			if (i % 2) array[i][j] = j + 1;
#endif // _DEBUG
		}
}
void printArray() {
	printf("array\n");
	for (int i = 0; i < ARRAY_SIZE_ROWS; i++) {
		for (int j = 0; j < ARRAY_SIZE_COLS; j++)
			printf("%4d ", array[i][j]);
		printf("\n");
	}
}
int main(){
	srand(time(0));
	randomizeArray();
	printArray();
	int k = 0;
	printf("Print shift value "); scanf_s("%d", &k);
	for (int i = 1; i < ARRAY_SIZE_ROWS; i += 2) {
		int tmp[ARRAY_SIZE_COLS] = { 0 };
		for (int j = 0; j < ARRAY_SIZE_COLS; j++)
				tmp[(j + k) % ARRAY_SIZE_COLS] = array[i][j];
		for (int j = 0; j < ARRAY_SIZE_COLS; j++) {
			array[i][j] = tmp[j];
		}
	}
	printf("result ");
	printArray();
	return 0;
}