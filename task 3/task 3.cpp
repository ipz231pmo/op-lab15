﻿#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#define ARRAY_SIZE_ROWS 5
#define ARRAY_SIZE_COLS 10
unsigned array[ARRAY_SIZE_ROWS][ARRAY_SIZE_COLS];

void randomizeArray();
void printArray();
void clamp(int min, int max, int* value);
void selectionSort(unsigned array[], int n);
void insertionSort(unsigned array[], int n);

int main() {
	srand(time(0));
	randomizeArray();
	printArray();	
	unsigned tmp[ARRAY_SIZE_COLS * ARRAY_SIZE_ROWS] = { 0 };
	for (int i = 0; i < ARRAY_SIZE_ROWS; i++)
		for (int j = 0; j < ARRAY_SIZE_COLS; j++)
			tmp[i * ARRAY_SIZE_COLS + j] = array[i][j];

	int sortChoice; 
	printf("Enter number of sortFunction\n");
	printf("1. insertion sort\n");
	printf("2. selection sort\n");
	scanf_s("%d", &sortChoice); clamp(1, 2, &sortChoice);
	switch (sortChoice){
	case 1:
		insertionSort(tmp, ARRAY_SIZE_COLS * ARRAY_SIZE_ROWS);
		break;
	case 2:
		selectionSort(tmp, ARRAY_SIZE_COLS * ARRAY_SIZE_ROWS);
		break;
	default:
		break;
	}
	for (int i = 0; i < ARRAY_SIZE_ROWS * ARRAY_SIZE_COLS; i++)
		array[i / ARRAY_SIZE_COLS][i % ARRAY_SIZE_COLS] = tmp[i];
	printf("result ");printArray();
	return 0;
}
void selectionSort(unsigned arr[], int n) {
	for (int i = 0; i < n; i++){
		int mini = i;
		for (int j = i + 1; j < n; j++)
			if (arr[j] < arr[mini]) mini = j;
		unsigned temp = arr[i];
		arr[i] = arr[mini];
		arr[mini] = temp;
	}
}
void clamp(int minv, int maxv, int* value) {
	if (*value < minv) *value = minv;
	if (*value > maxv) *value = maxv;
}
void insertionSort(unsigned array[], int n) {
	unsigned c;
	for (int i = 1; i < n; i++)
	{
		c = array[i];
		for (int j = i - 1; j >= 0 && array[j] > c; j--)
		{
			array[j + 1] = array[j];
			array[j] = c;
		}
	}
}
void printArray() {
	printf("array\n");
	for (int i = 0; i < ARRAY_SIZE_ROWS; i++) {
		for (int j = 0; j < ARRAY_SIZE_COLS; j++)
			printf("%4d ", array[i][j]);
		printf("\n");
	}
}
void randomizeArray() {
	for (int i = 0; i < ARRAY_SIZE_ROWS; i++)
		for (int j = 0; j < ARRAY_SIZE_COLS; j++)
			array[i][j] = rand() % 101;
}