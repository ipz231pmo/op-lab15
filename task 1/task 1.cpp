﻿#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define ARRAY_SIZE 10
int array[ARRAY_SIZE][ARRAY_SIZE];
void randomizeArray() {
	for (int i = 0; i < ARRAY_SIZE; i++)
		for (int j = 0; j < ARRAY_SIZE; j++)
			array[i][j] = rand() % 201 - 100;
}
void printArray() {
	printf("array\n");
	for (int i = 0; i < ARRAY_SIZE; i++) {
		for (int j = 0; j < ARRAY_SIZE; j++)
			printf("%4d ", array[i][j]);
		printf("\n");
	}
}
void sortArray(int array[]) {
	for (int i = 0; i < ARRAY_SIZE - 1; i++) {
		int mini = i;
		for (int j = i + 1; j < ARRAY_SIZE; j++)
			if (array[mini] > array[j]) mini = j;
		int tmp = array[mini];
		array[mini] = array[i];
		array[i] = tmp;
	}
}
int main(){
	srand(time(0));
	randomizeArray();
	printArray();
	for (int i = 0; i < ARRAY_SIZE; i += 2)
		sortArray(array[i]);
	printf("result ");
	printArray();
}
